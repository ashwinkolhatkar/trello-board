from . import db


class List(db.Model):
    """
    Corresponds to list table in the database and fields corresponds
    to the columns.
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    description = db.Column(db.String(200))
    tasks = db.relationship("Task", backref="list", lazy='dynamic')
