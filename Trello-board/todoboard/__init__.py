from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from todoboard.models import db
import config
import os


def create_app():
    template_dir = os.path.abspath('todoboard/views')
    app = Flask(__name__, template_folder=template_dir)
    app.config['SECRET_KEY'] = config.flask['SECRET_KEY']
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + config.mysql[
        'username'] + ':' + config.mysql['password'] + '@' + config.mysql[
            'host'] + ':' + config.mysql['port'] + '/' + config.mysql['dbname']

    app.app_context().push()
    db.init_app(app)

    from todoboard.controllers.main_controller import main_route
    from todoboard.controllers.list_controller import lists
    from todoboard.controllers.task_controller import task
    app.register_blueprint(main_route)
    app.register_blueprint(lists)
    app.register_blueprint(task)

    return app
