from flask import Blueprint
from flask import render_template, jsonify, request
from todoboard import db
from todoboard.models import List

lists = Blueprint('lists', __name__)


@lists.route('/list/delete/<int:list_id>', methods=['POST'])
def deleteList(list_id):
    '''
    This route queries the database for list_element by list_id and deletes it.
    If the list_id is not found it returns a 404.
    '''
    list_element = List.query.get_or_404(list_id)
    db.session.delete(list_element)
    db.session.commit()
    return 'OK', 200


@lists.route('/list/<int:list_id>/task/count', methods=['POST'])
def countTasks(list_id):
    '''
    This route queries the database for list_element by list_id and deletes it.
    If the list_id is not found it returns a 404.
    '''
    list_element = List.query.get_or_404(list_id)
    task_completed_count = list_element.tasks.filter_by(complete=True).count()
    task_tocomplete_count = list_element.tasks.filter_by(
        complete=False).count()
    print(task_completed_count, task_tocomplete_count)
    return jsonify({
        "list_id": list_id,
        "completed_count": task_completed_count,
        "tocomplete_count": task_tocomplete_count
    }), 200


@lists.route('/list/add', methods=['POST'])
def addList():
    '''
    This route gets json data about list and inserts it to the database
    The route returns the jsonified data containing
    id, name and description
    '''
    data = request.get_json()
    name = data['name']
    if len(name) > 25:
        return 'File Too Large', 413
    description = data['description']
    list_element = List(name=name, description=description)
    db.session.add(list_element)
    db.session.commit()
    return jsonify({
        "id": list_element.id,
        "name": name,
        "description": description
    }), 200
