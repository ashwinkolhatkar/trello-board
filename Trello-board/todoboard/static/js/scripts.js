/**
 * scripts.js contains all common functions
 */

$(document).ready(function() {
  $('.modal').modal();
  $('select').formSelect();
});
