/**
 * list.js contains all utility functions used for manipulating List
    elements
 */
function openModal(x) {
  $('#' + x).modal();
  $('select').formSelect();
}

function closeModal(x) {
  $('#' + x).modal('close');
}

$("#addListform").submit(function(event) {
  /*
  This function accepts the for data from form id addListform and
  sends to flask routes using AJAX.
  */
  var name = event.currentTarget[1].value;
  var description = event.currentTarget[2].value;
  var data = {
    name: name,
    description: description
  };

  $.ajax({
    type: "POST",
    url: '/list/add', // url to post to, from the forms action
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify(data),
    dataType: "json",
    success: function(data) {
      appendList(data);
      closeModal('addList');
    }
  })
  event.preventDefault();
});

$(document).on("submit", "#deleteList", function(event) {
  /*
  This function sends list_id from form deleteList to flask routes using AJAX
  and the deletes the list from frontend.
  */
  event.preventDefault();
  var list_id = event.currentTarget[0].value;
  $.ajax({
    type: "POST",
    url: '/list/delete/' + list_id,
    success: function() {
      $("#list" + list_id).remove();
      $("#List" + list_id).remove();
    }
  });
})

function appendList(list) {
  /*
  This function appends the list element and it's respective modal
  to the html <div> element class side-scroll.
  */

  var list_element = `
    <div class="card-scroller" id="list${list.id}">
    <div class="card-panel blue-grey lighten-1">
    <h3>
    <a class="modal-trigger" href="#List${list.id}" onClick="openModal('List${list.id}');">${list.name}</a>
    </h3>
    <!-- List Modal -->
    <div id="List` + list.id + `" class="modal">
      <div class="modal-content">
        <h4>${list.name}</h4>
        <p>${list.description}</p>
      </div>
      <form action="" id="deleteList" method="POST">
        <input type="hidden" id="listid"  name="listid" value="${list.id}">
        <input type="submit" id="deleteListButton" value="Delete List" class="button btn waves-effect waves-light red">
      </form>
      <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-red btn-flat">Close</a>
      </div>
    </div>
    <!-- end of modal -->

    <a class="waves-effect waves-light btn modal-trigger" onClick="openModal('addTaskModal${list.id}');" href="#addTaskModal${list.id}">Add Task</a>
      <!-- Add Task Modal -->
      <div id="addTaskModal${list.id}" class="modal" tabindex="0" style="z-index: 1003; display: none; opacity: 0; top: 4%; transform: scaleX(0.8) scaleY(0.8);">
        <div class="modal-content">
          <div>Add a new todo item:
            <form id="addTask" action="" method="POST">
              <input id="csrf_token" name="csrf_token" type="hidden" value="ImQ1MjkwOGFlYzQ2ZDllYzFhYmU4MGU1ZWNlNWFmM2NiZmNjOGE0Yzci.EKMnAw.kFSRAtJMSqaVtSRoEHiJrFEPBr8">
              <div class="form-group">
                <label class="form-control-label" for="name">Enter the name</label>
                <input class="form-control form-control-lg" id="name" name="name" required="" type="text" value="">
              </div>
              <div class="form-group">
                <label class="form-control-label" for="description">Enter a description</label>
                <input class="form-control form-control-lg" id="description" name="description" type="text" value="">
              </div>
              <label for="priority">Select Priority</label>
              <select><option value="low">Low</option><option value="medium">Medium</option><option value="high">High</option></select>
              <input type="hidden" id="listid"  name="listid" value="${list.id}">
              <div class="form-group">
                <input class="btn btn-outline-info button2" id="sendTask" type="submit" value="Add Task">
              </div>
            </form>
          </div>
        </div>
      </div>
      <!--end of modal -->
      <div class="card-panel grey lighten-1">
        <h5 id="ToComplete${list.id}">To complete: 0</h5>
        <ul id="ToCompleteList${list.id}">
        </ul>
      </div>
      <div class="card-panel grey lighten-1">
        <h5 id="Completed${list.id}">Completed: 0</h5>
        <ul id="CompletedList${list.id}">
        </ul>
      </div>
    </div>
  </div>`;
  $('#side-scroll').append(list_element);

}
