/**
 * task.js contains all utility functions used for manipulating task
    elements
 */
function openModal(x) {
  $('#' + x).modal();
  $('select').formSelect();
}

function closeModal(x) {
  $('#' + x).modal('close');
}

function appendCompletedTask(task) {
  /*
  This function appends the task element and it's respective modal
  to the html <ul> element with id CompletedList[0-9]+.
  */
  $.ajax({
    type: "POST",
    url: '/list/' + task.list_id + '/task/count', // url to post to, from the forms action
    success: function(data) {

      var tocomplete_count = data.tocomplete_count;
      var completed_count = data.completed_count;
      $("h5#ToComplete" + task.list_id).text('To complete: ' + tocomplete_count);
      $("h5#Completed" + task.list_id).text('Completed: ' + completed_count);
    }
  });



  var task_element = `
  <li id="task${task.id}">
    <div class="card-panel green lighten-3">
      <span class="card-title">
      <form action="" id="deleteTask" method="POST">
          ${task.name}
        <input type="hidden" id="taskid"  name="taskid" value="${task.id}">
        <input type="submit" value="Delete Task" class="button btn waves-effect waves-light red">
      </form>
      </span>
    </div>
  </li> `;
  $("ul#CompletedList" + task.list_id).append(task_element);
  $("#task" + task.id).remove();
}

function appendToCompleteTask(task) {
  /*
  This function appends the task element and it's respective modal
  to the html <ul> element with id ToCompleteList[0-9]+. Also decides
  which color the card should be based on it's priority.
  */
  $.ajax({
    type: "POST",
    url: '/list/' + task.list_id + '/task/count', // url to post to, from the forms action
    success: function(data) {
      var tocomplete_count = data.tocomplete_count;
      var completed_count = data.completed_count;
      $("h5#ToComplete" + task.list_id).text('To complete: ' + tocomplete_count);
      $("h5#Completed" + task.list_id).text('Completed: ' + completed_count);
    }
  });


  if (task.priority === "low")
    color = "grey lighten-3";
  else if (task.priority === "medium")
    color = "yellow lighten-3";
  else
    color = "red lighten-3";

  var task_element = `
    <li id="task${task.id}">
      <div class="card-panel ` + color + `">
        <span class="card-title"><a class= "modal-trigger" onClick="openModal('taskModal${task.id}');" href="#taskModal${task.id}">${task.name}</a></span>
      </div>
    </li>`;

  $("ul#ToCompleteList" + task.list_id).append(task_element);

  var task_modal = `
    <!-- Complete Task Modal -->
    <div id="taskModal${task.id}" class="modal">
      <div class="modal-content">
        <h3>${task.name}</h3>
        Description:
        <p>${task.description}</p>
        <form action="" id="deleteTask" method="POST">
          <input type="hidden" id="taskid"  name="taskid" value="${task.id}">
          <input type="submit" value="Delete" class="button btn waves-effect waves-light red">
        </form>
        <form action="" id="completeTask" method="POST">
          <input type="hidden" id="taskid"  name="taskid" value="${task.id}">
          <input type="submit" value="Complete" class="button btn waves-effect waves-light teal">
        </form>
        <a href="#!" class="modal-close waves-effect waves-red btn-flat">Close</a>
      </div>
    </div>`;

  $("ul#ToCompleteList" + task.list_id).append(task_modal);
}

$(document).on("submit", "#addTask", function(event) {
  /*
  This function accepts the for data from form id addTask and
  sends to flask routes using AJAX.
  */
  event.preventDefault();
  var name = event.currentTarget[1].value;
  var description = event.currentTarget[2].value;
  var priority = event.currentTarget[4].value;
  var list_id = event.currentTarget[5].value;
  data = {
    name: name,
    description: description,
    priority: priority
  };
  $.ajax({
    type: "POST",
    url: '/list/' + list_id + '/task/add/', // url to post to, from the forms action
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify(data),
    dataType: "json",
    success: function(data) {
      appendToCompleteTask(data);
      closeModal("addTaskModal" + list_id);
    }
  });
})

$(document).on("submit", "#completeTask", function(event) {
  /*
  This function sends list_id from form completeTask to flask routes using AJAX
  and then sets task.complete property to true and
  calls function appendCompletedTask on success to change the frontend
  */
  event.preventDefault();
  var task_id = event.currentTarget[0].value;
  $.ajax({
    type: "POST",
    url: '/task/complete/' + task_id,
    success: function(data) {
      appendCompletedTask(data);
      closeModal("taskModal" + task_id);
    }
  });
})

$(document).on("submit", "#deleteTask", function(event) {
  /*
  This function sends list_id from form deleteTask to flask routes using AJAX
  and the deletes the task and it's respective modal from frontend.
  */
  event.preventDefault();
  var task_id = event.currentTarget[0].value;
  $.ajax({
    type: "POST",
    url: '/task/delete/' + task_id,
    success: function(data) {
      $.ajax({
        type: "POST",
        url: '/list/' + data.list_id + '/task/count', // url to post to, from the forms action
        success: function(data) {
          $("h5#ToComplete" + data.list_id).text('To complete: ' + data.tocomplete_count);
          $("h5#Completed" + data.list_id).text('Completed: ' + data.completed_count);
        }
      });
      $("#task" + task_id).remove();
      $("#taskModal" + task_id).remove();
    }
  });
})
